#include <iostream>
#include "maillage.h"

int main()
{
    Maillage m("Th-50-0.9.msh");
    float p[] = {0., 0.};
    //float p[] = {0.75, 0.2}; // Tri: 0
    //float p[] = {0.65, 0.2}; // Tri: 24
    int i, t, s;
    t = m.triangle(p, 0);
    std::cout << "Tri: " << t << std::endl;
    for(i = 0; i < 3; i++) {
        s = m.triangles_[3 * t + i];
        std::cout << s << " (" << m.sommets_[2 * s + 0] << ", " << m.sommets_[2 * s + 1] << ")" << std::endl;
    }
    return 0;
}
