#!/usr/bin/env python

# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
# from matplotlib.colors import LinearSegmentedColormap


def read_mesh(name):
    f = open(name, 'r')
    lines = f.read().split('\n')
    f.close()
    line = lines[0].split(' ')
    num_vertices = int(line[0])
    num_triangles = int(line[1])
    vertices = np.empty((num_vertices, 2), dtype=np.float32)
    triangles = np.empty((num_triangles, 3), dtype=np.int32)
    for i in range(num_vertices):
        line = lines[i + 1].split(' ')
        for j in range(2):
            vertices[i, j] = float(line[j])
    for i in range(num_triangles):
        line = lines[num_vertices + i + 1].split(' ')
        for j in range(3):
            triangles[i, j] = int(line[j])
    triangles -= 1
    # print(vertices, triangles)
    return (vertices, triangles)


if __name__ == '__main__':
    vertices, triangles = read_mesh('Th-50-0.9.msh')
    point = (0., 0.)
    path = [0,31,321 ,21,319,28, 25,352,176,393,392 ,387,192,182]
    color = np.zeros((triangles.shape[0],), dtype=np.int8)
    color[path] = 1
    cm = matplotlib.colors.ListedColormap(['w', 'r'])
    fig = plt.figure()
    plt.gca().set_aspect('equal')
    x = vertices[:, 0]
    y = vertices[:, 1]
    # plt.triplot(x, y, triangles)
    plt.tripcolor(x, y, triangles, facecolors=color, edgecolors='b', cmap=cm)
    for t in range(triangles.shape[0]):
        tri = triangles[t, :]
        bary = (np.sum(x[tri]) / 3., np.sum(y[tri]) / 3.)
        plt.text(bary[0], bary[1], u"%i" % t, fontsize=10, fontweight='light',
                 ha='center', va='center')
    plt.scatter(point[0], point[1], color='lime')
    plt.show()
