#include <iostream>
#include <set>
#include "maillage.h"

// Ce programme affiche une liste de couple sommet de m1 et triangle de m0.
int main()
{
    Maillage m0("Th-50-0.9.msh"), m1("Th-50-0.8.msh");
    float p[2];
    std::set<int> sommets; // Pour ne pas traiter plusieurs fois le meme sommets
    int i, t0 = 0, t1, nt, s;
    nt = m1.tri_sommets_.size();
    for(t1 = 0; t1 < nt; t1++) {
        for(i = 0; i < 3; i++) {
            s = m1.triangles_[3 * t1 + i];
            if(sommets.find(s) != sommets.end()) {
                continue;
            }
            sommets.insert(s);
            p[0] = m1.sommets_[2 * s + 0];
            p[1] = m1.sommets_[2 * s + 1];
            if(t0 < 0)
                t0 = 0;
            t0 = m0.triangle(p, t0);
            std::cout << s << " " << t0 << std::endl;
        }
    }
    return 0;
}
