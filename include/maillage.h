#ifndef MAILLAGE_H
#define MAILLAGE_H

#include <iostream>
#include <vector>
#include <string>
#include <set>

class Maillage
{
    public:
    Maillage();
    Maillage(const std::string &name);
    int triangle(const float *p, int t, std::set<int> *list=nullptr) const;

    //private:
    std::vector<float> sommets_;
    std::vector<int> triangles_;
    std::vector<std::vector<int>> tri_sommets_; //std::vector<std::vector<int> > tri_sommets_;
};

#endif // MAILLAGE_H
