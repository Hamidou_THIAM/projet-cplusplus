#include <iostream>
#include "maillage.h"

int main()
{
    int i, t;
    Maillage m("Th-50-0.9.msh");
    std::cout << "Hello world" << std::endl;
    t = 0;
    std::cout << "Tri: " << t << std::endl;
    for(i = 0; i < 3; i++)
        std::cout << m.triangles_[3 * t + i] << std::endl;
    t = m.tri_sommets_[0][0];
    std::cout << "Tri: " << t << std::endl;
    for(i = 0; i < 3; i++)
        std::cout << m.triangles_[3 * t + i] << std::endl;
    t = m.tri_sommets_[24][2];
    std::cout << "Tri: " << t << std::endl;
    return 0;
}
