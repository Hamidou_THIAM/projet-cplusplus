#include "maillage.h"
#include <map>
#include <set>
#include <fstream>

Maillage::Maillage()
{
}

Maillage::Maillage(const std::string &name)
{
    int i, j, tmp, u, v, w;
    int num_vertices = 0, num_triangles = 0;
    float x, y;
    std::fstream file;
    std::map<std::set<int>, int> sommet_triangle;
    std::set<int> edge;                                            // ensemble à deux éléments
    std::map<std::set<int>, int>::iterator it;
    file.open(name, std::fstream::in);
    if(!file.is_open()) {
        std::cerr << "Unable to open file " << name << std::endl;
        return;
    }
    file >> num_vertices >> num_triangles >> tmp;                  // lecture des sommets et triangles
    for(i = 0; i < num_vertices; i++) {
        file >> x >> y >> tmp;                                    // remplissage du tableau sommets_
        sommets_.push_back(x);
        sommets_.push_back(y);
    }
    for(i = 0; i < num_triangles; i++) {
        file >> u >> v >> w >> tmp;
        u -= 1;
        v -= 1;
        w -= 1;
        // Un triangle.
        triangles_.push_back(u);                                  // replissage du tableau des triangles
        triangles_.push_back(v);
        triangles_.push_back(w);
        tri_sommets_.push_back(std::vector<int>(3, -1));
        // Associe un arete a un triangle.
        edge.clear();
        edge.insert(u);
        edge.insert(v);
        it = sommet_triangle.find(edge);
        if(it != sommet_triangle.end()) {
            tri_sommets_[i][2] = it->second;
            for(j = 0; j < 3; j++) {
                if(triangles_[3 * it->second + j] == u)
                    continue;
                if(triangles_[3 * it->second + j] == v)
                    continue;
                if(triangles_[3 * it->second + j] == w)
                    continue;
                break;
            }
            tri_sommets_[it->second][j] = i;
            sommet_triangle.erase(it);
        }
        else {
            sommet_triangle.insert(std::make_pair<std::set<int>, int>(std::set<int>(edge), int(i)));
        }
        edge.clear();
        edge.insert(u);
        edge.insert(w);
        it = sommet_triangle.find(edge);
        if(it != sommet_triangle.end()) {
            tri_sommets_[i][1] = it->second;
            for(j = 0; j < 3; j++) {
                if(triangles_[3 * it->second + j] == u)
                    continue;
                if(triangles_[3 * it->second + j] == v)
                    continue;
                if(triangles_[3 * it->second + j] == w)
                    continue;
                break;
            }
            tri_sommets_[it->second][j] = i;
            sommet_triangle.erase(it);
        }
        else {
            sommet_triangle.insert(std::make_pair<std::set<int>, int>(std::set<int>(edge), int(i)));
        }
        edge.clear();
        edge.insert(w);
        edge.insert(v);
        it = sommet_triangle.find(edge);
        if(it != sommet_triangle.end()) {
            tri_sommets_[i][0] = it->second;
            for(j = 0; j < 3; j++) {
                if(triangles_[3 * it->second + j] == u)
                    continue;
                if(triangles_[3 * it->second + j] == v)
                    continue;
                if(triangles_[3 * it->second + j] == w)
                    continue;
                break;
            }
            tri_sommets_[it->second][j] = i;
            sommet_triangle.erase(it);
        }
        else {
            sommet_triangle.insert(std::make_pair<std::set<int>, int>(std::set<int>(edge), int(i)));
        }
    }
}

float det2(float *u, float *v)
{
    return u[0] * v[1] - u[1] * v[0];
}

int Maillage::triangle(const float *p, int t, std::set<int> *list) const
{
    int i, ii, j = 0;
    int ret = t;
    float u[2], v[2];
    int to_visite[] = {-1, -1, -1};
    bool allocate = false;
    if(list == nullptr) {
        list = new std::set<int>();
        allocate = true;
    }
    if(list->find(t) != list->end())
        return -1;
    list->insert(t);
    /* Print the set.
    std::set<int>::iterator it;
    for(it = list->begin(); it != list->end(); it++)
        std::cout << *it << std::endl;
    // */
    if(t < 0) {
        if(allocate)
            delete list;
        return -1;
    }
    //std::cout << "Here: " << t << std::endl;
    for(i = 0; i < 3; i++) {                       // calcul des coordonnées des vecteurs
        ii = (i + 1) % 3;
        u[0] = sommets_[2 * triangles_[3 * t + ii] + 0] - sommets_[2 * triangles_[3 * t + i] + 0];
        u[1] = sommets_[2 * triangles_[3 * t + ii] + 1] - sommets_[2 * triangles_[3 * t + i] + 1];
        v[0] = p[0] - sommets_[2 * triangles_[3 * t + ii] + 0];
        v[1] = p[1] - sommets_[2 * triangles_[3 * t + ii] + 1];
        if(det2(u, v) < -1e-6) {
            ret = Maillage::triangle(p, tri_sommets_[t][(ii + 1) % 3], list);
            //ret = Maillage::triangle(p, tri_sommets_[t][(ii + 1) % 3]);
            if(ret >= 0)
                break;
        }
        else {
            to_visite[j] = (ii + 1) % 3;
            j++;
        }
    }
    if(ret >= 0) {
        if(allocate)
            delete list;
        return ret;
    }
    for(i = 0; i < j; i++) {
        ret = Maillage::triangle(p, tri_sommets_[t][to_visite[i]], list);
        //ret = Maillage::triangle(p, tri_sommets_[t][to_visite[i]]);
        if(ret >= 0) {
            if(allocate)
                delete list;
            return ret;
        }
    }
    if(allocate)
        delete list;
    return -1; // No triangle found.
}
